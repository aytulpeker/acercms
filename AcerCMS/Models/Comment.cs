﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AcerCMS.Models
{
    public class Comment
    {
        public int Id { get; set; }
        
        [Display(Name="Adınız")]
        [Required(ErrorMessage = "{0} Gerekli!")]
        [StringLength(50, ErrorMessage = "{0} en fazla {1} karakter uzunluğunda olabilir!")]
        public string Author { get; set; }
        public DateTime PostDate { get; set; }

        [Display(Name="Yorumunuz")]
        [Required(ErrorMessage = "{0} Gerekli!")]
        [StringLength(400, ErrorMessage = "{0} en fazla {1} karakter uzunluğunda olabilir!")]
        public string Content { get; set; }
    }
}