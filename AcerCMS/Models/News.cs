﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AcerCMS.Models
{
    public class News
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string Content { get; set; }
    }
}