﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AcerCMS.Models;

namespace AcerCMS.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ReadNews(int id)
        {
            var model = new NewsViewModel()
            {
                News = new News()
                {
                    Id = 1,
                    Content = "Bu Bir deneme Haberdir!",
                    ShortDescription = "Kısa Açıklama",
                    Title = "Çoook Önemli bir Haber"
                },
                Comments = new List<Comment>()
                {
                    new Comment()
                    {
                        Content = "çok önemliymiş",
                        Id = 1,
                        Author = "Mesut Talebi",
                        PostDate = DateTime.Now
                    },
                    new Comment()
                    {
                        Content = "çok önemliymiş2",
                        Id = 2,
                        Author = "kdaj kjh kjhkjhs",
                        PostDate = DateTime.Now.AddHours(1)
                    },
                    new Comment()
                    {
                        Content = "çok önemliymiş3",
                        Id = 3,
                        Author = " sadf asdf sadf sadf asdf",
                        PostDate = DateTime.Now.AddHours(2)
                    },
                    new Comment()
                    {
                        Content = "çok önemliymiş4",
                        Id = 4,
                        Author = "Mesut Talebiq qw dfsa",
                        PostDate = DateTime.Now
                    },
                    new Comment()
                    {
                        Content = "çok önemliymiş dfasd fsd f",
                        Id = 5,
                        Author = "Mesut Talebi adsfsdf adsf",
                        PostDate = DateTime.Now
                    }
                }
            };
           
            return View(model);
        }
    }
}